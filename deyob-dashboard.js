var express = require('express')
    , http = require('http')
    , path = require('path')
    , app = express()

// all environments
app.set('port', process.env.PORT || 14051);

var dir = "prod";
if (app.get('env')=='dist') {
	dir = "dist";
	app.use(express.static(path.join(__dirname, './dist')));
}
else if (app.get('env')=='prod') app.use(express.static(path.join(__dirname, './prod')));
else app.use(express.static(path.join(__dirname, './prod')));

http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port %d in %s mode", app.get('port'), dir);
});
