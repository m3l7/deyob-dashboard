(function(){
  angular
    .module('app')
    .config(function ($routeProvider) {
      $routeProvider
        .when('/dashboard', {
          templateUrl: 'pages/dashboard.html',
          controller: 'Dashboard',
          data:{
            roles: ['admin','brand', 'dashboard']
          }
        })
        .when('/brands/:id', {
          templateUrl: 'pages/brand.html',
          controller: 'Brand',
          data:{
            roles: ['admin']
          }
        })
        .when('/brands', {
          templateUrl: 'pages/brands.html',
          controller: 'Brands',
          data:{
            roles: ['admin']
          }
        })
        .when('/invalidators', {
          templateUrl: 'pages/invalidators.html',
          controller: 'Invalidators',
          data:{
            roles: ['admin']
          }
        })
        .when('/products', {
          templateUrl: 'pages/products.html',
          controller: 'Products',
          data:{
            roles: ['admin']
          }
        })
        .when('/products/:id', {
          templateUrl: 'pages/product.html',
          controller: 'Product',
          data:{
            roles: ['admin']
          }
        })
        .when('/campaigns/:id', {
          templateUrl: 'pages/campaign.html',
          controller: 'Campaign',
          data:{
            roles: ['admin','brand']
          }
        })
        .when('/campaigns', {
          templateUrl: 'pages/campaigns.html',
          controller: 'Campaigns',
          data:{
            roles: ['admin','brand']
          }
        })
        .when('/codes/bulk/:idBegin/:idEnd', {
          templateUrl: 'pages/codesBulk.html',
          controller: 'CodesBulk',
          data:{
            roles: ['admin','brand']
          },
          resolve:{
            viewType: function(){return 'bulk';}
          }
        })
        // .when('/codes/bulk', {
        //   templateUrl: 'pages/codesBulk.html',
        //   controller: 'CodesBulk',
        //   data:{
        //     roles: ['admin','brand']
        //   },
        //   resolve:{
        //     viewType: function(){return 'bulk';}
        //   }
        // })
        // .when('/codes/new', {
        //   templateUrl: 'pages/codesNew.html',
        //   controller: 'CodesBulk',
        //   data:{
        //     roles: ['admin']
        //   },
        //   resolve:{
        //     viewType: function(){return 'new';}
        //   }
        // })
        .when('/codes/generate', {
          templateUrl: 'pages/codes/generate.html',
          controller: 'CodesGenerate',
          data:{
            roles: ['admin']
          }
        })
        .when('/codes/edit', {
          templateUrl: 'pages/codes/edit.html',
          controller: 'CodesEdit',
          data:{
            roles: ['admin']
          }
        })
        .when('/codes/export', {
          templateUrl: 'pages/codesExport.html',
          controller: 'CodesExport',
          data:{
            roles: ['admin','brand']
          },
        })
        // .when('/codes', {
        //   templateUrl: 'pages/codes.html',
        //   controller: 'Codes',
        //   data:{
        //     roles: ['admin','brand']
        //   }
        // })
        .when('/news', {
          templateUrl: 'pages/news.html',
          controller: 'News',
          data:{
            roles: ['admin']
          }
        })
        .when('/notifications', {
          templateUrl: 'pages/notifications.html',
          controller: 'Notifications',
          data:{
            roles: ['admin']
          }
        })
        .when('/login', {
          templateUrl: 'admin/login.html',
          controller: 'Login',
        })
        .when('/users/export', {
          templateUrl: 'pages/usersExport.html',
          controller: 'UsersExport',
          data:{
            roles: ['admin']
          },
        })
        .otherwise({
          redirectTo: '/dashboard'
        });
    });  
})();
