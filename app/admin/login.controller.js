(function(){
	angular
		.module('app')
		.controller('Login',Login);

		Login.$inject = ['$scope','$timeout','$location', 'security'];

		function Login($scope,$timeout,$location, security){

			$scope.login = login;
			$scope.authError = "";
			$scope.login = login;

			initialize();

			function initialize(){
				if (security.isAuthenticated) security.logout();
			}

			function login(){
                var user = $scope.username;
                var pass = $scope.password;
                
                security.login(user,pass,function(){
                    if (security.isAuthenticated){
                        $location.path('/');
                    }
                    else{
                        $scope.authError = 'Nome utente o password non validi';
                        $timeout(function(){
                            $scope.authError = '';
                        },5000)
                    }
                });

            }

		}
})();