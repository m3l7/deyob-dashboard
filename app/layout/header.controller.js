(function(){
    angular
        .module('app')
        .controller('Header',Header);

        Header.$inject = ['$scope','config'];

        function Header($scope,config){
            $scope.config = config;
        }
})();