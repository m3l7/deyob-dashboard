(function(){
	angular
		.module('app')
		.controller('Nav',Nav);

		Nav.$inject = ['$scope','$location', 'config','security', 'Utils', 'utilUser', "$sce"];

		function Nav($scope,$location,config,security, Utils, utilUser, $sce){
			$scope.config = config;
			$scope.security = security;
			$scope.logout = logout;
			$scope.Utils = Utils;
			$scope.exportUsers = exportUsers;
			$scope.exportLink = null;

			function exportUsers(){
				utilUser.getExportUsersLink()
				.then(function(link){
					$scope.exportLink = $sce.trustAsResourceUrl(link);
				})
				.catch(function(err){
					console.warn("generate export user link: "+err);
				});
			}


			function logout(){
				security.logout();
				$location.path("/login");
			}
		}
})();