(function(){
	angular
		.module('app')
		.controller('Brand',Brand);

		Brand.$inject = ['$scope','$routeParams','$location','Upload', 'userModel','config','logService'];

		function Brand($scope,$routeParams,$location,Upload, userModel,config,logService){
			initialize();

			$scope.save = save;
			$scope.myImage='';
			$scope.myCroppedImage='';
			$scope.imageEdited = false;
			$scope.baseUrlAssets = config.API.baseUrlAssets;
			$scope.invalidId = false;
			$scope.brand = {};
			$scope.logService = logService;


			function save(){

				$scope.brand.role = 'brand';

				var blob;
				if (!!$scope.myCroppedImage) blob = dataURItoBlob($scope.myCroppedImage)

				var method = ($routeParams.id=='new') ? 'post': 'put';
				var id = $scope.brand.id || '';

				for (var i in $scope.brand){
					if (!$scope.brand[i]) delete $scope.brand[i];
				}

                Upload.upload({
                  url: config.API.baseUrl+'user/'+id,
                  method: method,
                  headers: {'Authorization': 'bearer '+localStorage.getItem("token")}, // only for html5
                  file: blob, // single file or a list of files. list is only for html5,
                  fields: $scope.brand
                }).success(function(data, status, headers, config) {
                    if (status==200){
                    	userModel.inject(data);
				        $location.path('/brands');
			        }
			        
                });


			}


			function handleFileSelect(evt) {

			  var file=evt.currentTarget.files[0];
			  var reader = new FileReader();
			  reader.onload = function (evt) {
			    $scope.$apply(function($scope){
			      $scope.imageEdited = true;
			      $scope.myImage=evt.target.result;
			    });
			  };
			  console.log($scope.myImage)
			  reader.readAsDataURL(file);

			};

            function dataURItoBlob(dataURI, callback) {
			    var byteCharacters = atob(dataURI.split(',')[1]);
			    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
			    var byteNumbers = new Array(byteCharacters.length);
				for (var i = 0; i < byteCharacters.length; i++) {
				    byteNumbers[i] = byteCharacters.charCodeAt(i);
				}
				var byteArray = new Uint8Array(byteNumbers);
				return new Blob([byteArray], {type: mimeString});
			}

			function initialize(){
				var id;
				if ((id=$routeParams.id)!='new'){
					//fetch brand
					userModel.find(id)
					.then(function(brand){
						if ((!brand) || (brand.role!='brand')) {
							$scope.invalidId = true;
							$scope.alert = {
								type: 'danger',
								text: 'ID non valido'
							}
						}
					})
					userModel.bindOne($scope,'brand',id);
				}

				angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
			}
		}
})();