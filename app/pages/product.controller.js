(function() {
    angular
        .module("app")
        .controller("Product", Product);

    Product.$inject = [
        "$scope",
        "$routeParams",
        "productModel",
        "Upload",
        "logService",
        "config",
        "imageproductModel",
        "userModel",
        "$q"
    ];

    function Product($scope, $routeParams, productModel, Upload, logService, config, imageproductModel, userModel, q) {

        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $scope.product = {};
        $scope.save = save;
        $scope.format = 'dd.MM.yyyy';
        $scope.openDateStart = openDateStart;
        $scope.addImage = addImage;
        $scope.deleteImage = deleteImage;
        $scope.dataImage = {};
        $scope.imagesToDelete = []; // support array to keep track of images to delete (already saved on db)
        $scope.addImageLocal = addImageLocal; // add image locally to save at the end
        $scope.imagesToSave = []; // support array to keep track of new images to save on db
        $scope.deleteImageLocal = deleteImageLocal; // delete image locally to delete at the end
        $scope.initialStateProduct = {};
        $scope.cancel = cancel;


        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            class: 'datepicker'
        };

        initialize();

        function openDateStart($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.openedStart = true;
        };

        function addImage(imageToSaveURI) {
            if (imageToSaveURI) {

                var blob = dataURItoBlob(imageToSaveURI);

                return Upload.upload({
                    url: config.API.baseUrl + 'imageproduct', // upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {
                        'Authorization': 'bearer ' + localStorage.getItem("token")
                    }, // only for html5
                    file: blob, // single file or a list of files. list is only for html5,
                    fields: {
                        product: $scope.product.id
                    }
                }).success(function(data, status, headers, config) {
                    if ((status == 200) || (status == 201)) {
                        if (!$scope.product.images) $scope.product.images = [];
                        $scope.product.images.push(data);
                    }
                });
            }
        }

        function saveImages() {

            return q.all($scope.imagesToSave.map(function(image) {
                return addImage(image);
            }))




        }

        function deleteImages() {
            return q.all($scope.imagesToDelete.map(function(image) {
                return deleteImage(image);
            }))
        }

        function addImageLocal() {
            if ($scope.myCroppedImage) {
                $scope.imageEdited = false;
                var blob = dataURItoBlob($scope.myCroppedImage);

                $scope.dataImage.path = $scope.myCroppedImage;
                $scope.dataImage.id = null;
                $scope.dataImage.product = null;

                if (!$scope.product.images) $scope.product.images = [];
                // Add image (for view)
                $scope.product.images.push($scope.dataImage);
                // Add image to temp array of images to save at the end
                $scope.imagesToSave.push($scope.myCroppedImage);

                $scope.myCroppedImage = '';
                $scope.dataImage = {};

            }
        }

        function deleteImage(image) {
            if (!!image) {
                var id = image.id;
                if (id) {
                    imageproductModel.inject(image);
                    return imageproductModel.destroy(id)
                        .then(function() {
                            $scope.product.images.forEach(function(img, i) {
                                if (img.id == id) $scope.product.images.splice(i, 1);
                            })
                            var product = productModel.get($scope.product.id);
                            if (!!product) product.images.forEach(function(img, i) {
                                if (img.id == id) product.images.splice(i, 1);
                            })
                            $scope.alert = {
                                type: "success",
                                text: "Immagine eliminata con successo"
                            };
                        })
                }
            }
        }

        function deleteImageLocal(image) {
            if (!!image) {
                if (image.id) {
                    // if the image is already on db, then delete it
                    deleteImage(image);

                } else {
                    var path = image.path;
                    $scope.product.images.forEach(function(img, i) {
                        if (img.path == path) $scope.product.images.splice(i, 1);
                    })
                    $scope.imagesToDelete.push(image); // add image to delete list
                    $scope.imagesToSave.forEach(function(img, i) {
                        if (img == path) $scope.imagesToSave.splice(i, 1); // remove image from to save list
                    })
                }


            }
        }

        function save() {
            console.log('in save()');
            var id = $routeParams.id;
            $scope.saveLabelObj.disable = true;
            $scope.saveLabelObj.text = 'Salvando...';

            if ($scope.product.link) {
                if ($scope.product.link.substring(0, 7) != 'http://' && $scope.product.link.substring(0, 8) != 'https://') {
                    $scope.product.link = 'http://' + $scope.product.link;
                }
            }

            delete $scope.product.images;

            var action = (id == "new") ? "create" : "save";
            productModel[action]($scope.product)
                .then(function(product) {
                    if (product) {
                        $scope.product.id = product.id;
                    }
                })
                .then(saveImages)
                .then(deleteImages)
                .then(function() {
                    console.log('after saveImages');
                    delete $scope.imagesToSave;
                    delete $scope.imagesToDelete;

                    return productModel.find($scope.product.id, {
                        bypassCache: true
                    });
                })
                .then(function() {
                    console.log('end save: brand product=' + $scope.product.brand);
                    initialize();
                    $scope.alert = {
                        type: "success",
                        text: "Prodotto salvato con successo"
                    };
                })
                .catch(function(err) {
                    $scope.alert = {
                        type: "error",
                        text: "Errore salvando le modifiche:"
                    };
                    initialize();
                })

        }

        function handleFileSelect(evt) {

            $scope.imageEdited = false;
            $scope.myImage = '';
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.imageEdited = true;
                    $scope.myImage = evt.target.result;
                });
            };
            console.log($scope.myImage)
            reader.readAsDataURL(file);

        };

        function dataURItoBlob(dataURI, callback) {
            var byteCharacters = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            return new Blob([byteArray], {
                type: mimeString
            });
        }

        function initialize() {
            var id;
            if ((id = $routeParams.id) != 'new') {
                //fetch campaign
                productModel.findAll({
                    id: id
                }, {
                    bypassCache: true
                }).then(function(product) {
                    console.log('inside then after findAll, product=' + product);
                    if (product.length) {
                        // if js-data re-map the object in array 
                        $scope.product = sanitizeProduct(product[0]);
                        console.log('brand product=' + $scope.product.brand);
                        $scope.initialStateProduct = {};
                        $scope.initialStateProduct = angular.copy($scope.product);
                    } else if (product) {
                        console.log('inside then after findAll, into if');
                        $scope.product = sanitizeProduct(product);
                        console.log('brand product=' + $scope.product.brand);
                        $scope.initialStateProduct = {};
                        $scope.initialStateProduct = angular.copy($scope.product);
                    }

                });

            } else {
                $scope.imageEdited = false;
            }

            $scope.saveLabelObj = {
                text: 'Salva Cambiamenti',
                disable: false
            };

            console.log('product=' + $scope.product.brand);

            userModel.findAll({
                role: "brand"
            });
            userModel.bindAll($scope, "brands", {
                role: "brand"
            });

            $scope.imagesToSave = [];
            $scope.imagesToDelete = [];
            $scope.dataImage = {};
            $scope.myImage = '';
            $scope.myCroppedImage = '';



            angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

        }

        function sanitizeProduct(product) {
            if (product) {
                if (typeof product.brand == "object") product.brand = product.brand.id;
                return product;
            }
        }


        function cancel() {
            initialize();
            // $scope.product = angular.copy($scope.initialStateProduct);
            // $scope.imagesToSave = [];
            // $scope.imagesToDelete = [];
            // $scope.dataImage = {};
            // $scope.myImage = '';
            // $scope.myCroppedImage = '';
        }


    }
})();