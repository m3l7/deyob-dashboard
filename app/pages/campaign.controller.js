(function() {
    angular
        .module('app')
        .controller('Campaign', Campaign);

    Campaign.$inject = ['$scope', '$routeParams', '$location', 'campaignModel', 'userModel', 'Upload', 'config', '$modal', 'awardModel'];

    function Campaign($scope, $routeParams, $location, campaignModel, userModel, Upload, config, $modal, awardModel) {

        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $scope.imageEdited = false;
        $scope.dtStart = new Date();
        $scope.dtStop = new Date();
        $scope.campaign = {
            type: 1,
            visible: true
        };
        $scope.save = save;
        $scope.campaignTypes = config.campaignTypes;
        $scope.format = 'dd.MM.yyyy';
        $scope.openDateStart = openDateStart;
        $scope.openDateStop = openDateStop;
        $scope.newCampaign = false;

        $scope.editAward = editAward;
        $scope.deleteAward = deleteAward;

        initialize();

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            class: 'datepicker'
        };

        function openDateStart($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.openedStart = true;
        };

        function openDateStop($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.openedStop = true;
        };



        function save() {
            var id = $routeParams.id;

            var brand = (typeof $scope.campaign.brand == 'object') ? $scope.campaign.brand.id : $scope.campaign.brand;

            delete $scope.campaign.brand;
            delete $scope.campaign.awards;
            if (!!brand) $scope.campaign.brand = brand;

            //rebuild typeName
            config.campaignTypes.forEach(function(type) {
                if (type.type == $scope.campaign.type) $scope.campaign.typeName = type.typeName;
            })

            if($scope.campaign.link){
                if ($scope.campaign.link.substring(0, 7) != 'http://') {
                    $scope.campaign.link = 'http://' + $scope.campaign.link;
                }
            }   
            if (id != 'new') {
                campaignModel.save($scope.campaign)
                    .then(postSave);
            } else {
                campaignModel.create($scope.campaign)
                    .then(function(campaign) {
                        $scope.campaign = campaign;
                        postSave();
                    })
            }
        }

        function postSave() {
            //UGLY SHIT -- SHOULD USE ONE SINGLE CALL FOR FORM AND UPLOAD IMAGE
            //upload image
            if (($scope.imageEdited) && (!!$scope.myCroppedImage)) {
                var blob = dataURItoBlob($scope.myCroppedImage)
                Upload.upload({
                    url: config.API.baseUrl + 'campaign/' + $scope.campaign.id + '/image', // upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {
                        'Authorization': 'bearer ' + localStorage.getItem("token")
                    }, // only for html5
                    file: blob, // single file or a list of files. list is only for html5,
                }).success(function(data, status, headers, config) {
                    if (status == 200) {
                        $scope.campaign.image = data;
                    }
                    $location.path('/campaigns');
                });
            } else $location.path('/campaigns');
        }

        function handleFileSelect(evt) {

            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.imageEdited = true;
                    $scope.myImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);

        };

        function dataURItoBlob(dataURI, callback) {
            var byteCharacters = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            return new Blob([byteArray], {
                type: mimeString
            });
        }

        function deleteAward(award) {
            if (!!award) {
                awardModel.destroy(award);
            }
        }

        function editAward(award) {

            if (!award) award = {
                campaign: $scope.campaign.id
            }

            var modalInstance = $modal.open({
                templateUrl: '/components/modals/editAward.html',
                controller: 'EditAwardModal',
                resolve: {
                    award: function() {
                        return award;
                    },
                    campaign: function() {
                        return $scope.campaign;
                    }
                }
            });

            modalInstance.result.then(function(award) {
                if (!!award) {
                    //check if we should upload a new image
                    $scope.blob = award.blob;
                    delete award.blob;

                    if (!!award.id) {
                        awardModel.inject(award);
                        awardModel.save(award.id).then(postSave);
                    } else {
                        //create new
                        awardModel.create(award).then(postSave);
                    }

                    function postSave(award) {
                        if (!!$scope.blob) {
                            var blob = dataURItoBlob($scope.blob);
                            Upload.upload({
                                url: config.API.baseUrl + 'award/' + award.id + '/image', // upload.php script, node.js route, or servlet url
                                method: 'POST',
                                headers: {
                                    'Authorization': 'bearer ' + localStorage.getItem("token")
                                }, // only for html5
                                file: blob, // single file or a list of files. list is only for html5,
                            }).success(function(data, status, headers, config) {
                                if (status == 200) {
                                    award.image = data;
                                    awardModel.inject(award);
                                }
                            });

                        }
                    }
                }
            })

        }

        function initialize() {
            var id;
            if ((id = $routeParams.id) != 'new') {
                //fetch campaign
                campaignModel.findAll({
                        id: id,
                        // populate: 'awards,brand'
                        populate: 'awards'
                    })
                    .then(function(campaigns) {
                        if (campaigns.length) {

                            var campaign = campaigns[0];

                            if (typeof campaign.brand == 'object') campaign.brand = campaign.brand.id;

                            awardModel.ejectAll();
                            if ((campaign) && (campaign.awards)) campaign.awards.forEach(function(award) {
                                awardModel.inject(award);
                            });
                        }
                    })
                campaignModel.bindOne($scope, 'campaign', id);
                awardModel.bindAll($scope, 'awards');

            } else $scope.newCampaign = true;

            userModel.ejectAll();
            userModel.findAll({
                role: 'brand'
            });
            userModel.bindAll($scope, 'brands');

            angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

        }
    }
})();