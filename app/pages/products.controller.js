(function(){
	angular
		.module("app")
		.controller("Products",Products);

		Products.$inject = ["$scope","productModel","$location"];

		function Products($scope,productModel,$location){
			$scope.editSelected = editSelected;
			$scope.deleteSelected = deleteSelected;

			$scope.gridOptions = {
			  enableFiltering: true,
			  showFooter: true,
			  rowHeight: 36,
			  enableRowSelection: true,
			  headerHeight: 77,
			  offsetTop: 75,
			  enableRowHeaderSelection: false,
			  multiSelect: false,
			  onRegisterApi: onRegisterApi,
			  // onRegisterApi: onRegisterApi,
			  columnDefs: [
			    { name: 'Nome', field:'name' },
			    { name: 'Scadenza',field: 'expire' },
			    { name: 'Punti',field:'points' },
			    { name: 'Brand',field:'brand.name' },
			  ],
			  data: []
			};

			initialize();

			function initialize(){

				productModel.findAll()
				.then(function(products){
					$scope.gridOptions.data = products;
				});
				
			}

			function onRegisterApi(gridApi){
			      //set gridApi on scope
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){});

			};

			function editSelected(){
				var sel = $scope.gridApi.selection.getSelectedRows();
				if (sel.length){
					$location.path('/products/'+sel[0].id);
				}
			}
			function deleteSelected(){
				var sel = $scope.gridApi.selection.getSelectedRows();
				if (sel.length){
					var id = sel[0].id;
					productModel.destroy(sel[0].id)
					.then(function(){
						$scope.gridOptions.data.forEach(function(product,i){
							if (product.id==id) $scope.gridOptions.data.splice(i,1);
						})
					})
				}
			}


		}
})();