(function() {
    angular
        .module('app')
        .controller('Dashboard', Dashboard);

    Dashboard.$inject = ['$scope', '$http', 'config', 'dashboardModel', 'campaignModel', 'codeblockModel', 'userModel', 'security'];

    function Dashboard($scope, $http, config, dashboardModel, campaignModel, codeblockModel, userModel, security) {


        $scope.filter = {};
        $scope.scanTotal = 0;
        $scope.locations = [];
        $scope.lastCodesView = false; //true if we are viewing last codes instead of general stats
        $scope.changeMap = changeMap;
        $scope.lastCodesMenu = getLastCodesMenu();
        $scope.filterChanged = filterChanged;
        $scope.rangeCollapsed = false;


        // DETAIL MAP OPTIONS
        $scope.detailMap = {
            center: {
                latitude: 41.9027830,
                longitude: 12.4963660
            },
            zoom: 5
        };
        $scope.detailMapMarker = [];

        // LAST CODES TABLE OPTIONS
        $scope.gridOptions = {
            rowHeight: 40,
            enableRowHeaderSelection: false,
            multiSelect: false,
            columnDefs: [{
                name: '',
                field: 'user.image',
                cellTemplate: "<img class='user-image' ng-src='{{COL_FIELD}}'/>",
                width: 40
            }, {
                name: 'Nome',
                field: 'user.first_name'
            }, {
                name: 'Cognome',
                field: 'user.last_name'
            }, {
                name: 'Eta',
                field: 'user.age'
            }, {
                name: 'Email',
                field: 'user.email'
            }, {
                name: 'Indirizzo',
                field: 'street'
            }, {
                name: 'Citta',
                field: 'location'
            }, {
                name: 'Campagna',
                field: 'campaign.name'
            }, {
                name: 'Data',
                field: 'scanDate',
                cellTemplate: "<span>{{COL_FIELD | amDateFormat:'DD/MM/YY HH:mm'}}</span>"
            }, ],
            onRegisterApi: onRegisterApi,
            data: []
        };

        // DATE RAGE PICKER OPTIONS
        $scope.datePickerOptions = {
            ranges: {
                'Oggi': [moment(), moment()],
                'Ieri': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimi 7 giorni': [moment().subtract(6, 'days'), moment()],
                'Ultimi 30 giorni': [moment().subtract(29, 'days'), moment()],
                'Ultimo anno': [moment().subtract(1, 'year').startOf('month'), moment().endOf('month')]
            },
        }


        //CHARTS OPTIONS
        $scope.genderData = [];
        $scope.newUserData = [];
        $scope.osData = [];
        $scope.plotData = [];
        $scope.plotOptions = {
            colors: ['#23b7e5', '#fad733'],
            series: {
                shadowSize: 2
            },
            xaxis: {
                timezone: "browser",
                mode: 'time',
                minTickSize: [1, 'hour'],
                font: {
                    color: '#ccc'
                }
            },
            yaxis: {
                font: {
                    color: '#ccc'
                }
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0,
                color: '#ccc'
            },
            tooltip: true,
            tooltipOpts: {
                content: '%s of %x.1 is %y.4',
                defaultTheme: false,
                shifts: {
                    x: 0,
                    y: 20
                }
            },
            timezone: "browser"
        };
        $scope.genderOptions = {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.5,
                    stroke: {
                        width: 0
                    },
                    label: {
                        show: true,
                        threshold: 0.05
                    }
                }
            },
            colors: ['#27c24c', '#23b7e5'],
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0,
                color: '#ccc'
            },
            tooltip: true,
            tooltipOpts: {
                content: '%s: %p.0%'
            }
        };
        $scope.osOptions = {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.5,
                    stroke: {
                        width: 0
                    },
                    label: {
                        show: true,
                        threshold: 0.05
                    }
                }
            },
            colors: ['#27c24c', '#23b7e5', '#f05050'],
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0,
                color: '#ccc'
            },
            tooltip: true,
            tooltipOpts: {
                content: '%s: %p.0%'
            }
        };
        $scope.ageOptions = {
            colors: ['#23b7e5'],
            series: {
                shadowSize: 2
            },
            xaxis: {
                font: {
                    color: '#ccc'
                }
            },
            yaxis: {
                font: {
                    color: '#ccc'
                },
                min: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0,
                color: '#ccc'
            },
            // tooltip: true,
            // tooltipOpts: { content: '%s of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 0, y: 20 } }
        };
        $scope.mapOptions = {
            container: $('#geo-map'),
            map: 'world_mill_en',
            markers: [],
            zoomOnScroll: false,
            normalizeFunction: 'polynomial',
            backgroundColor: '#fff',
            icon: "https://lh4.ggpht.com/Tr5sntMif9qOPrKV_UVl7K8A_V3xQDgA7Sw_qweLUFlg76d_vGFA7q1xIKZ6IcmeGqg=w300",
            regionStyle: {
                initial: {
                    _fill: '#e8eff0',
                    fill: '#E5EAEB'
                },
                hover: {
                    fill: '#8d83c6'
                },
            },
            markerStyle: {
                initial: {
                    fill: '#23b7e5',
                    stroke: '#23b7e5'
                }
            },
            onMarkerLabelShow: formatMapLabel
        };



        initialize();

        function initialize() {
            $scope.filter.campaign = 'all';
            $scope.filter.brand = 'all';
            $scope.filter.codeblock = 'all';
            $scope.filter.range = rangeToObj('day');
            loadStats($scope.filter);

            var filter = (security.user.role == 'brand') ? {
                brand: security.user.uid
            } : {};

            campaignModel.ejectAll();
            campaignModel.findAll(filter);
            campaignModel.inject({
                id: 'all',
                name: 'Tutte le campagne'
            })
            campaignModel.bindAll($scope, 'campaigns');

            userModel.ejectAll();
            userModel.findAll({
                role: "brand"
            })
            userModel.inject({
                id: 'all',
                name: 'Tutti i brand'
            })
            userModel.bindAll($scope, 'brands');

            codeblockModel.ejectAll();
            codeblockModel.inject({
                id: 'all',
                description: 'Tutti i codici'
            })
            codeblockModel.bindAll($scope, 'codeblocks');
        }



        function filterChanged(value) {
            if (value == "lastCodes") {
                $scope.rangeCollapsed = true;
            } else if (value == "range") {
                $scope.rangeCollapsed = false;
                delete $scope.filter.lastCodes;
            } else if (value == "brand") {
                var filter = ($scope.filter.brand == "all") ? {} : {
                    brand: $scope.filter.brand
                };
                campaignModel.ejectAll();
                campaignModel.findAll(filter);
                campaignModel.inject({
                    id: 'all',
                    name: 'Tutte le campagne'
                })
                campaignModel.bindAll($scope, 'campaigns');
            } else if (value == "campaign") {
                if ($scope.filter.campaign) {
                    var filter = ($scope.filter.campaign == "all") ? {} : {
                        campaign: $scope.filter.campaign
                    };
                    codeblockModel.ejectAll();
                    codeblockModel.findAll(filter);
                    codeblockModel.inject({
                        id: 'all',
                        description: 'Tutti i codici'
                    })
                    codeblockModel.bindAll($scope, 'codeblocks');
                }

            }

            //reload data
            if (!!$scope.filter.lastCodes) loadLastCodesStat($scope.filter);
            else loadStats($scope.filter);
        }

        function getLastCodesMenu() {
            return [{
                value: 10,
                name: "Ultimi 10 codici"
            }, {
                value: 100,
                name: "Ultimi 100 codici"
            }, {
                value: 1000,
                name: "Ultimi 1000 codici"
            }, ]
        }

        function onRegisterApi(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {

                $scope.detailMapMarker.forEach(function(marker) {
                    if (row.entity.id == marker.id) marker.icon = "content/images/red_marker.png";
                    else marker.icon = "content/images/blue_marker.png";
                })
            });

        };

        function changeMap(mapType) {
            if (!!mapType) {
                $scope.mapOptions.map = mapType;
                $scope.map = $('#geo-map').vectorMap('get', 'mapObject');
                $scope.map.remove();
                $('#geoMap').empty();
                $scope.map = $('#geo-map').vectorMap($scope.mapOptions);
            }
        }

        function rangeToTime(range) {
            //convert string range to timestamp range
            var to = new Date();
            var from = new Date();
            if (range == 'day') {
                // from.setDate(from.getDate()-1);
                from.setHours(0);
                from.setMinutes(0);
            } else if (range == 'week') from.setDate(from.getDate() - 7);
            else if (range == 'month') from.setMonth(from.getMonth() - 1);
            else if (range == 'year') from.setFullYear(from.getFullYear() - 1);
            else if (range == 'null') from.setFullYear(1990); //ugly workaround for setting a different date
            else from = null;

            var time = '';
            if (!!from) time = from.getTime() + '-' + to.getTime();


            return time;
        }

        function rangeToObj(range) {
            //convert string range to timestamp range
            var to = new Date();
            var from = new Date();

            if (range == 'day') {
                from.setHours(0);
                from.setMinutes(0);
            } else if (range == 'week') from.setDate(from.getDate() - 7);
            else if (range == 'month') from.setMonth(from.getMonth() - 1);
            else if (range == 'year') from.setFullYear(from.getFullYear() - 1);
            else if (range == 'null') from.setFullYear(1990); //ugly workaround for setting a different date
            else from = null;
            var time = '';
            if (!!from) time = {
                startDate: from,
                endDate: to
            };

            return time;
        }

        function rangeObjToTime(range) {
            return (range.startDate.getTime() + '-' + range.endDate.getTime());
        }

        function loadStats(filter) {

            var filter = angular.copy(filter);
            if (filter.campaign == "all") delete filter.campaign;
            if (filter.brand == "all") delete filter.brand;
            if (filter.codeblock == "all") delete filter.codeblock;
            if (typeof filter.range == "object") filter.range = rangeObjToTime(filter.range);

            $scope.lastCodesView = false;

            dashboardModel.ejectAll();
            dashboardModel.findAll(filter)
                .then(function(data) {


                    data = (data.length) ? data[0] : {};

                    $scope.countries = data.countries;
                    $scope.scanTotal = data.total;
                    $scope.campaign = data.campaign;
                    $scope.totalNew = data.totalNew;

                    //build countries and cities data
                    var countries = {},
                        cities = {};
                    if (!!data.locations) data.locations.forEach(function(loc) {
                        if (!countries[loc.countryCode]) {
                            countries[loc.countryCode] = {
                                countryCode: loc.countryCode,
                                country: loc.country,
                                value: 1
                            }
                        } else countries[loc.countryCode].value++;

                        if (!cities[loc.name]) {
                            cities[loc.name] = {
                                name: loc.name,
                                value: 1
                            }
                        } else cities[loc.name].value++;
                    })

                    $scope.countries = [], $scope.cities = [];
                    for (var i in countries) {
                        $scope.countries.push(countries[i]);
                    }
                    for (var i in cities) {
                        if ((i.length > 0) && (i != "null")) $scope.cities.push(cities[i]);
                    }
                    //rebuild new User Chart
                    $scope.newUserData = [{
                        label: 'nuovi utenti',
                        data: data.totalNew
                    }, {
                        label: 'utenti di ritorno',
                        data: data.totalReturning
                    }, ];
                    $.plot('#new-user-chart', $scope.newUserData, $scope.genderOptions);

                    //rebuild gender Chart
                    if (!data.male) data.male = 0;
                    if (!data.female) data.female = 0;
                    var total = data.male + data.female;
                    var malePercentage = (total) ? data.male / total * 100 : 0;
                    var femalePercentage = (total) ? data.female / total * 100 : 0;
                    malePercentage = Math.round(malePercentage * 10) / 10;
                    femalePercentage = Math.round(femalePercentage * 10) / 10;

                    $scope.genderData = {
                        malePercentage: malePercentage,
                        femalePercentage: femalePercentage
                    }

                    //rebuild os Chart
                    $scope.osData = [{
                        label: 'iOS',
                        data: data.os.ios
                    }, {
                        label: 'android',
                        data: data.os.android
                    }, {
                        label: 'Windows Phone',
                        data: data.os.winphone
                    }, ];
                    $.plot('#os-chart', $scope.osData, $scope.osOptions);

                    //rebuild age Chart
                    $scope.ageData = [{
                        data: data.ages,
                        bars: {
                            show: true,
                            barWidth: 10,
                            fillColor: {
                                colors: [{
                                    opacity: 0.2
                                }, {
                                    opacity: 0.4
                                }]
                            }
                        }
                    }];
                    $.plot('#age-chart', $scope.ageData, $scope.ageOptions);

                    //rebuild plot chart
                    $scope.plotData = [{
                        data: data.dates,
                        label: 'Scansioni totali',
                        points: {
                            show: true
                        },
                        lines: {
                            show: true,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.1
                                }, {
                                    opacity: 0.1
                                }]
                            }
                        }
                    }, {
                        data: data.datesNew,
                        label: 'Nuovi utenti',
                        points: {
                            show: true,
                            radius: 4
                        },
                        lines: {
                            show: true,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.1
                                }, {
                                    opacity: 0.1
                                }]
                            }
                        }
                    }];
                    var range = $scope.filter.range;
                    console.log((range.endDate - range.startDate) / 1000 / 3600)
                    if ((range.endDate - range.startDate) / 1000 / 3600 <= 24) $scope.plotOptions.xaxis.minTickSize = [1, 'hour'];
                    else $scope.plotOptions.xaxis.minTickSize = [1, 'day'];
                    $.plot('#scan-plot', $scope.plotData, $scope.plotOptions);

                    //rebuild geo map
                    if (!$scope.map) {
                        $scope.mapOptions.markers = data.locations;
                        $scope.map = $('#geo-map').vectorMap($scope.mapOptions);
                    } else {
                        $scope.map = $('#geo-map').vectorMap('get', 'mapObject');
                        $scope.mapOptions.markers = data.locations;
                    }

                })
                .catch(function(err, status) {

                })
        }

        function loadLastCodesStat(filter) {
            //load last num scans

            var filter = angular.copy(filter);
            if (filter.campaign == "all") delete filter.campaign;
            if (filter.brand == "all") delete filter.brand;
            if (filter.codeblock == "all") delete filter.codeblock;
            delete filter.range;
            var num = filter.lastCodes;
            delete filter.lastCodes;

            $http.get(config.API.baseUrl + 'dashboard/last/' + num, {
                    params: filter
                })
                .success(function(data, status) {
                    if (status == 200) {
                        if (!!data) {
                            $scope.lastCodesView = true;

                            $scope.detailMapMarker = data

                            //group locations
                            var locationsObj = {};
                            data.forEach(function(code) {

                                if (!code.user) code.user = {
                                    email: "",
                                    first_name: "",
                                    last_name: "",
                                    address: ""
                                }
                                if ((!!code.user) && (!code.user.image)) code.user.image = "content/images/user_placeholder.png";

                                if ((!!code.lat) && (!!code.lng)) {

                                    code.icon = "content/images/blue_marker.png",

                                        code.latitude = code.lat;
                                    code.longitude = code.lng
                                        //normalize geocodes
                                        // code.lat = normalizeGeo(code.lat);
                                        // code.lng = normalizeGeo(code.lng);

                                    // var geostring = code.lat.toString()+code.lng.toString();

                                    // if (!!locationsObj[geostring]) locationsObj[geostring].count++;
                                    // else locationsObj[geostring] = {
                                    // 	count: 1,
                                    // 	latLng: [
                                    // 		code.lat,
                                    // 		code.lng
                                    // 	],
                                    // 	name: "<div style='width: 100%;margin-bottom:10px;'><strong>"+code.location+"</strong></div>"
                                    // };
                                    // locationsObj[geostring].name+="<div style='width: 50%; height: 25px; float: left;'><img style='width: 20px;margin-bottom: 5px; margin-right:5px;' src='"+code.user.image+"'/>"+code.user.first_name+" "+code.user.last_name+", "+code.user.age+"</div>";
                                }
                            });

                            var locations = [];
                            for (var i in locationsObj) {
                                locations.push(locationsObj[i]);
                            }

                            //rebuild geo map
                            // if (!$scope.map){
                            // 	$scope.mapOptions.markers = locations;
                            // 	$scope.map = $('#geo-map').vectorMap($scope.mapOptions);
                            // }
                            // else{
                            // 	$scope.map = $('#geo-map').vectorMap('get','mapObject');
                            // 	$scope.map.removeAllMarkers();
                            // 	$scope.map.addMarkers(locations);
                            // 	$scope.mapOptions.markers = locations;
                            // }

                            //populate table
                            $scope.gridOptions.data = data;

                        }
                    }
                })
                .error(function(data, status) {

                })
        }

        function normalizeGeo(geo) {
            //truncate geo to 0.05 precision.
            //i.e.  45.5687 -> 45.55 (~10km precision)
            geo = Math.trunc(geo * 100);
            var mod = geo % 5;
            var geo = (mod > 2) ? geo + (5 - mod) : geo - mod;
            return (geo / 100);
        }

        function formatMapLabel(event, label, index) {
            if ($scope.lastCodesView) label.html('<div style="width: 400px;">' + label.text() + "</div>")
        }


    }
})();