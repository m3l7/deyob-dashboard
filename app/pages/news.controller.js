(function() {
    angular
        .module('app')
        .controller('News', News);

    News.$inject = ['$scope', 'config', 'newsModel', 'logService', 'lodash', 'imagenewsModel', 'Upload', '$q'];

    function News($scope, config, newsModel, logService, _, imagenewsModel, Upload, q) {

        $scope.showDetail = false;
        $scope.isNew = false;
        $scope.addNews = addNews;
        $scope.cancel = cancel;
        $scope.save = save;
        $scope.newsDetailObj = {};
        $scope.logService = logService;
        $scope.selectNews = selectNews;
        $scope.deleteNews = deleteNews;
        $scope.addImage = addImage;
        $scope.deleteImage = deleteImage;
        $scope.myCroppedImage = '';
        $scope.config = config;
        $scope.dataImage = {};
        $scope.imageEdited = false;
        $scope.imagesToDelete = []; // support array to keep track of images to delete (already saved on db)
        $scope.addImageLocal = addImageLocal; // add image locally to save at the end
        $scope.imagesToSave = []; // support array to keep track of new images to save on db
        $scope.deleteImageLocal = deleteImageLocal; // delete image locally to delete at the end
        $scope.initialStateNews = {};




        initialize();

        function initialize() {
            newsModel.findAll();
            newsModel.bindAll($scope, 'newsList');

            addNews();


            $scope.imagesToSave = [];
            $scope.imagesToDelete = [];
            $scope.dataImage = {};
            $scope.myImag = '';
            $scope.myCroppedImage = '';
            $scope.saveLabelObj = {
                text: 'Salva Cambiamenti',
                disable: false
            };

            $scope.imageEdited = false;

            angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
        }

        function handleFileSelect(evt) {
            $scope.imageEdited = false;
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.imageEdited = true;
                    $scope.myImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);

        };

        function saveImages() {

            return q.all($scope.imagesToSave.map(function(image) {
                return addImage(image);
            }))
        }

        function deleteImages() {

            return q.all($scope.imagesToDelete.map(function(image) {
                return deleteImage(image);
            }))

        }

        function save(valid) {
            $scope.formSubmitted = true;

            if ($scope.newsDetailObj.link) {
                if ($scope.newsDetailObj.link.substring(0, 7) != 'http://' && $scope.newsDetailObj.link.substring(0, 8) != 'https://') {
                    $scope.newsDetailObj.link = 'http://' + $scope.newsDetailObj.link;
                }
            }

            if (valid) {

                $scope.saveLabelObj.disable = true;
                $scope.saveLabelObj.text = 'Salvando...';

                if (!$scope.newsDetailObj.id) {
                    //create new news
                    newsModel.create($scope.newsDetailObj)
                        .then(function(newsSaved) {
                            $scope.newsDetailObj = angular.copy(newsSaved);
                        })
                        .then(saveImages)
                        .then(deleteImages)
                        .then(function() {
                            return newsModel.find($scope.newsDetailObj.id, {
                                bypassCache: true
                            });
                        })
                        .then(function(newsDetail) {
                            initialize();
                            selectNews(newsDetail);
                            $scope.alert = {
                                type: "success",
                                text: "Notizia salvata con successo"
                            }
                        })
                        .then(newsModel.findAll)
                        .catch(function(err) {
                            $scope.alert = {
                                type: "error",
                                text: "Errore salvando le modifiche:"
                            };
                            initialize();
                        })
                } else {
                    //update
                    var news = angular.copy($scope.newsDetailObj);
                    delete news.images;
                    newsModel.update(news.id, news)
                        .then(saveImages)
                        .then(deleteImages)
                        .then(function() {
                            return newsModel.find($scope.newsDetailObj.id, {
                                bypassCache: true
                            });
                        })
                        .then(function(newsDetail) {
                            initialize();
                            selectNews(newsDetail);
                            $scope.alert = {
                                type: "success",
                                text: "Notizia salvata con successo"
                            };
                        })
                        .then(newsModel.findAll)
                        .catch(function(err) {
                            $scope.alert = {
                                type: "error",
                                text: "Errore salvando le modifiche:"
                            };
                            initialize();
                        })
                }

            }
        }


        function addNews() {
            $scope.showDetail = true;
            $scope.formSubmitted = false;
            $scope.isNew = true;
            $scope.newsDetailObj = {
                title: '',
                description: '',
                html: ''
            }
        }

        function cancel() {
            if ($scope.isNew) $scope.newsDetailObj = {};
            else {
                var newsListObj = _.indexBy($scope.newsList, 'id');
                $scope.newsDetailObj = angular.copy(newsListObj[$scope.newsDetailObj.id]);
            }
            return false;
        }

        function selectNews(news) {
            if (!!news) {
                $scope.isNew = false;
                // $scope.newsDetailObj = news;
                $scope.newsDetailObj = angular.copy(news);
                $scope.showDetail = true;
                $scope.imageEdited = false;
                $scope.imagesToDelete = []; // support array to keep track of images to delete (already saved on db)
                $scope.imagesToSave = []; // support array to keep track of new images to save on db
                // $scope.initialStateNews = angular.copy($scope.newsDetailObj);
                delete $scope.myImage;
                $scope.saveLabelObj = {
                    text: 'Salva Cambiamenti',
                    disable: false
                };
                angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

            }
        }

        function deleteNews() {
            if (!!$scope.newsDetailObj.id) {
                newsModel.destroy($scope.newsDetailObj.id)
                    .then(function() {
                        $scope.alert = {
                            type: "success",
                            text: "Notizia eliminata con successo"
                        };
                        $scope.newsDetailObj = {};
                        $scope.showDetail = false;
                    })
            }
        }

        function deleteImage(image) {
            if (!!image) {
                var id = image.id;
                if (id) {
                    imagenewsModel.inject(image);
                    return imagenewsModel.destroy(id)
                        .then(function() {
                            $scope.newsDetailObj.images.forEach(function(img, i) {
                                if (img.id == id) $scope.newsDetailObj.images.splice(i, 1);
                            })
                            var news = newsModel.get($scope.newsDetailObj.id);
                            if (!!news) news.images.forEach(function(img, i) {
                                if (img.id == id) news.images.splice(i, 1);
                            })
                            $scope.alert = {
                                type: "success",
                                text: "Immagine eliminata con successo"
                            };
                        })
                }
            }
        }

        function dataURItoBlob(dataURI, callback) {
            var byteCharacters = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            return new Blob([byteArray], {
                type: mimeString
            });
        }

        function addImage(imageToSaveURI) {
            if (imageToSaveURI) {
                var blob = dataURItoBlob(imageToSaveURI)
                return Upload.upload({
                    url: config.API.baseUrl + 'imagenews', // upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {
                        'Authorization': 'bearer ' + localStorage.getItem("token")
                    }, // only for html5
                    file: blob, // single file or a list of files. list is only for html5,
                    fields: {
                        news: $scope.newsDetailObj.id
                    }
                }).then(function(data, status, headers, config) {
                    if ((status == 200) || (status == 201)) {
                        var news = newsModel.get($scope.newsDetailObj.id);
                        if (!!news) news.images.push(data);
                    }
                });
            }
        }


        function addImageLocal() {
            if ($scope.myCroppedImage) {
                var blob = dataURItoBlob($scope.myCroppedImage);

                $scope.imageEdited = false;

                console.log('blob: ' + blob);

                $scope.dataImage.path = $scope.myCroppedImage;
                $scope.dataImage.id = null;
                $scope.dataImage.news = null;

                if (!$scope.newsDetailObj.images) $scope.newsDetailObj.images = [];
                // Add image (for view)
                $scope.newsDetailObj.images.push($scope.dataImage);
                // Add image to temp array of images to save at the end
                $scope.imagesToSave.push($scope.myCroppedImage);

                $scope.myCroppedImage = '';
                $scope.dataImage = {};

            }
        }


        function deleteImageLocal(image) {
            if (!!image) {
                if (image.id) {
                    // if the image is already on db, then delete it
                    deleteImage(image);

                } else {
                    var path = image.path;
                    $scope.newsDetailObj.images.forEach(function(img, i) {
                        if (img.path == path) $scope.newsDetailObj.images.splice(i, 1);
                    })
                    $scope.imagesToDelete.push(image); // add image to delete list
                    $scope.imagesToSave.forEach(function(img, i) {
                        if (img == path) $scope.imagesToSave.splice(i, 1); // remove image from to save list
                    })
                }
            }
        }
    }

})();