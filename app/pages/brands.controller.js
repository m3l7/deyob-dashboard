(function(){
	angular
		.module('app')
		.controller('Brands',Brands);

		Brands.$inject = ['$scope','$window','$location', 'userModel', '$timeout'];

		function Brands($scope,$window,$location, userModel, $timeout){

			$scope.editSelected = editSelected;
			$scope.deleteSelected = deleteSelected;

			$scope.gridOptions = {
			  enableFiltering: true,
			  showFooter: true,
			  rowHeight: 36,
			  headerHeight: 77,
			  offsetTop: 75,
			  enableRowSelection: true,
			  enableRowHeaderSelection: false,
			  multiSelect: false,
			  onRegisterApi: onRegisterApi,
			  columnDefs: [
			    { name: 'Nome', field:'name' },
			    { name: 'Username', field:'username' },
			    { name: 'Billing City',field: 'location' },
			    { name: 'Sito Web',field:'link' },
			    { name: 'Telefono',field:'phone' },
			  ],
			  data: []
			};

			function editSelected(){
				var sel = $scope.gridApi.selection.getSelectedRows();
				if (sel.length){
					$location.path('/brands/'+sel[0].id);
				}
			}
			function deleteSelected(){
				var sel = $scope.gridApi.selection.getSelectedRows();
				if (sel.length){
					userModel.destroy(sel[0].id);
				}
			}

			function onRegisterApi(gridApi){
			      //set gridApi on scope
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){});
			 };


			initialize();

			function initialize(){
				userModel.findAll({role:'brand'});
				userModel.bindAll($scope,'gridOptions.data',{role:'brand'});
			}

		}
})();