(function(){
	angular
		.module("app")
		.controller("CodesGenerate",CodesGenerate);

		CodesGenerate.$inject = [
			"$scope",
			"$routeParams",
			"config",
			"$http",
			"security"
		];

		function CodesGenerate($scope,$routeParams,config,$http, security){

			//public properties
			$scope.codeTypes = getCodeTypes();
			$scope.isRangeValid = false;
			$scope.codeParams = {};

			//public methods
			$scope.rangeChanged = rangeChanged;
			$scope.suggestIdBegin = suggestIdBegin;
			$scope.idBeginChanged = idBeginChanged;
			$scope.generate = generate;

			initialize();

			function initialize(){
				$scope.codeParams = {
					exported: false
				}
				$scope.formSubmitted = false;
				$scope.codeSuggestion = "";
			}

			function rangeChanged(){
				suggestIdBegin();
				$scope.isRangeValid = true;
			}
			function idBeginChanged(){
				suggestCodeRange();
			}

			function getCodeTypes(){
				return [
					{
						name: 'Fisico',
						value: false
					},
					{
						name: "Digitale",
						value: true
					}
				];
			}

			function suggestIdBegin(){
				if ($scope.codeParams.range){

					$http.get(config.API.baseUrl+'codeblock/near',{params:{
						begin: 1,
						end: $scope.codeParams.range + 1
					}})
					.success(function(data,status){
						$scope.codeParams.idBegin = data.end+1;
					})
					
				}
			}

			function suggestCodeRange(){

					$http.get(config.API.baseUrl+'codeblock/near',{params:{
						begin:$scope.codeParams.idBegin,
						end:$scope.codeParams.idBegin+$scope.codeParams.range,
					}})
					.success(function(data,status){
						var idBegin = $scope.codeParams.idBegin;
						var range = $scope.codeParams.range;
						var idEnd = idBegin + range;
						if ((idBegin>data.end) && (idEnd<data.next)){
							$scope.isRangeValid = true;
							$scope.codeSuggestion = "ID/range valido";
						}
						else {
							$scope.isRangeValid = false;
							$scope.codeSuggestion = "ID/range non valido. ID libero piu vicino: "+(data.end+1)+" con "+data.emptyAfter+" codici liberi";
						}
					})
					.error(function(data,status){
						if (data.summary) $scope.alert = {type:'danger',text:"Errore: "+data.summary};
					})
			}

			function generate(valid){
				$scope.formSubmitted = true;
				if (valid){

					$http.post(config.API.baseUrlGenerateCodes+'code/bulk',{
						idBegin: $scope.codeParams.idBegin,
						idEnd: $scope.codeParams.idBegin + $scope.codeParams.range -1,
						exported: $scope.codeParams.exported
					})
					.success(function(data,status){
						if (status==200) $scope.alert = {type:'success',text:"Generazione codici iniziata. Controlla la mail   "+security.user.email+"   per aggiornamenti"};
						else if ((status==400) && (data.errorCode==5001)) $scope.alert = {type:'danger',text:"Impossibile modificare i codici. Range non valido"};
						else if ((status==400) && (data.errorCode==5002)) $scope.alert = {type:'danger',text:"Impossibile modificare tutti i codici. Modificati "+data.records+" codici."};
						else if (data.summary) $scope.alert = {type:'danger',text:"Errore: "+data.summary};
						else $scope.alert = {type:'danger',text:"Errore: Impossibile creare i codici"};

						initialize();
					})

				}
			}

		}
})();