(function() {
    angular
        .module('app')
        .controller('CodesEdit', CodesEdit);

    CodesEdit.$inject = [
        '$scope',
        '$location',
        'codeModel',
        'campaignModel',
        'userModel',
        'security',
        '$q',
        '$http',
        'config',
        "codeblockModel",
        "Upload",
        "productModel",
        "lodash"
    ];

    function CodesEdit($scope, $location, codeModel, campaignModel, userModel, security, $q, $http, config, codeblockModel, Upload, productModel, _) {


        $scope.deleteSelected = deleteSelected;
        $scope.codeFilter = {
            brand: {},
            campaign: {}
        };
        $scope.loadCodes = loadCodes;
        $scope.editBlock = editBlock;
        $scope.removeFilter = removeFilter;
        $scope.baseUrl = config.API.baseUrl;

        $scope.reloadBrandRelatedModels = reloadBrandRelatedModels;
        $scope.reloadCampaigns = reloadCampaigns;
        $scope.reloadFilterCampaigns = reloadFilterCampaigns;
        $scope.formatFilterCampaign = formatFilterCampaign;
        $scope.selectCampaign = selectCampaign;

        $scope.actionTypes = config.actionTypes,
            $scope.selectBlock = selectBlock;
        $scope.codeParams = {};
        $scope.codeFilter = {};
        $scope.assignBlocks = assignBlocks;
        $scope.deleteBlock = deleteBlock;
        $scope.saveBlock = saveBlock;
        $scope.selectAssignedFilter = selectAssignedFilter;
        $scope.config = config;
        $scope.imageEdited = false;
        $scope.actionCroppedImage = "";
        $scope.imagetmp = "";

        $scope.codeFilters = {
            brand: {
                id: 10002
            }
        }


        initialize();

        function initialize() {

            $scope.assignedFilter = true;

            userModel.findAll({
                role: "brand"
            });
            userModel.bindAll($scope, "brands", {
                role: "brand"
            });
            reloadFilterCampaigns();

            productModel.ejectAll();
            productModel.findAll();
            productModel.bindAll($scope, "products");

            $scope.campaignTypesIndex = _.indexBy(config.campaignTypes, "type");

            angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);

            reloadBlocks();

        }

        function selectAssignedFilter(filter) {
            if (filter) {
                if (filter == "assigned") codeblockModel.bindAll($scope, "blocks", {
                    assigned: true
                });
                if (filter == "unassigned") codeblockModel.bindAll($scope, "blocks", {
                    assigned: false
                });
                $scope.assignedFilter = filter;
            }
        }

        function saveBlock(block) {
            if (block) {
                var actionType = block.actionType;

                var blob;
                if (actionType) {
                    block.action = true;
                    if (!actionType) {
                        block.action = false;
                        block.actionText = "";
                    } else if ((actionType == 2) && ($scope.imageEdited)) blob = dataURItoBlob($scope.actionCroppedImage);
                } else {
                    block.action = false;
                    block.actionType = 0;
                    block.actionText = "";
                }

                for (var i in block) {
                    if (i != 'product') { // if this is property product then skip delete because can be null
                        if ((typeof block[i] != "number") && (!block[i])) {
                            delete block[i];
                        }
                    }
                }

                var uploadConfig = {
                    url: config.API.baseUrl + 'codeblock/' + block.id, // upload.php script, node.js route, or servlet url
                    method: "put",
                    fields: block,
                    headers: {
                        'Authorization': 'bearer ' + localStorage.getItem("token")
                    }, // only for html5
                }
                if (blob) uploadConfig.file = blob; // single file or a list of files. list is only for html5,

                Upload.upload(uploadConfig)
                    .success(function(data, status) {
                        if (status == 200) {
                            reloadBlocks();
                            $scope.alert = {
                                type: 'success',
                                text: 'blocco modificato con successo'
                            };
                        } else if ((status == 400) && (data.errorCode == 5001)) $scope.alert = {
                            type: 'danger',
                            text: "Impossibile modificare i codici. Range non valido"
                        };
                        else if ((status == 400) && (data.errorCode == 5002)) $scope.alert = {
                            type: 'danger',
                            text: "Impossibile modificare tutti i codici. Modificati " + data.records + " codici."
                        };
                        else if (data.summary) $scope.alert = {
                            type: 'danger',
                            text: "Errore: " + data.summary
                        };
                        else $scope.alert = {
                            type: 'danger',
                            text: "Errore: Impossibile creare i codici"
                        };

                    })
            }
        }

        function assignBlocks(codeParams) {
            if (codeParams) {

                $http.put(config.API.baseUrl + 'code/bulk', codeParams)
                    .success(function(data, status) {
                        if (status == 200) $scope.alert = {
                            type: 'success',
                            text: codeParams.range + ' codici modificati con successo'
                        };
                        else if ((status == 400) && (data.errorCode == 5001)) $scope.alert = {
                            type: 'danger',
                            text: "Impossibile modificare i codici. Range non valido"
                        };
                        else if ((status == 400) && (data.errorCode == 5002)) $scope.alert = {
                            type: 'danger',
                            text: "Impossibile modificare tutti i codici. Modificati " + data.records + " codici."
                        };
                        else if (data.summary) $scope.alert = {
                            type: 'danger',
                            text: "Errore: " + data.summary
                        };
                        else $scope.alert = {
                            type: 'danger',
                            text: "Errore: Impossibile creare i codici"
                        };
                        initialize();
                    })
            }
        }

        function deleteBlock(codeParams, id) {
            console.log('Block to delete='+id);

            if (codeParams && id) {

                $http.delete(config.API.baseUrl + 'codeblock/'+id)
                    .success(function(data, status) {
                        console.log('Block delete status='+status);
                        if (status == 200) $scope.alert = {
                            type: 'success',
                            text: codeParams.range + ' codici eliminati con successo'
                        };
                        else $scope.alert = {
                            type: 'danger',
                            text: "Errore: Impossibile eliminare i codici"
                        };
                        initialize();
                    })

                
            }
        }

        function selectBlock(block) {

            if (block) {

                var currentBlock = angular.copy(block);

                //delete relations from object
                if (typeof block.campaign == "object") {
                    currentBlock.campaignType = block.campaign.type;
                    currentBlock.campaign = block.campaign.id;
                }

                if (typeof block.product == "object") {
                    currentBlock.product = block.product.id;
                }

                if (typeof block.brand == "object") currentBlock.brand = block.brand.id;

                $scope.currentBlock = currentBlock;

                if (!block.assigned) {
                    $scope.codeParams = {
                        idBegin: block.begin,
                        range: block.end - block.begin + 1
                    }
                } else {
                    if (block.brand) reloadBrandRelatedModels(block.brand.id);
                    $scope.codeParams = {};
                }
            }
        }

        function buildEmbedYoutubeCode(link) {
            if (!!link) {
                //compute video link for iframe and thumb
                var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                var match = link.match(regExp);
                var videoId = match[7];
                if (match && match[7].length == 11) {
                    var embedLink = 'https://www.youtube.com/embed/' + videoId;
                    var thumb = 'http://img.youtube.com/vi/' + videoId + '/mqdefault.jpg';
                    return '<iframe src="' + embedLink + '" frameborder="0" allowfullscreen></iframe>';
                } else return '';
            } else return '';
        }


        function reloadBrandRelatedModels(brand) {
            if (brand) {
                reloadCampaigns(brand);
                productModel.findAll({
                    brand: brand,
                    populate: "none"
                });
                productModel.bindAll($scope, "products", {
                    brand: brand
                });
            }

        }

        function formatFilterCampaign() {
            var id = $scope.codeFilter.campaign.id;
            $scope.codeFilter.campaign = {
                id: id
            };
        }

        function selectCampaign(block) {
            if (block) {
                campaignModel.getAll().forEach(function(campaign) {
                    if (campaign.id == block.campaign) block.campaignType = campaign.type;
                })
            }
        }


        function reloadFilterCampaigns(brand) {
            if (brand) {
                campaignModel.findAll({
                    brand: brand,
                    populate: "none"
                });
                campaignModel.bindAll($scope, "filterCampaigns", {
                    brand: brand
                });
            } else {
                campaignModel.findAll({
                    populate: "none"
                });
                campaignModel.bindAll($scope, "filterCampaigns");
            }
        }

        function reloadCampaigns(brand) {
            if (brand) {
                campaignModel.findAll({
                        brand: brand,
                        populate: "none"
                    })
                    .then(function(campaigns) {
                        //build full name with campaign type name. 
                        //This should be unnecessary. This code is already in category.model.js
                        campaigns.forEach(function(campaign) {
                            campaign.typeName = ($scope.campaignTypesIndex[campaign.type]) ? $scope.campaignTypesIndex[campaign.type].typeName : "";
                            campaign.fullName = campaign.name + " (" + campaign.typeName + ")";
                        })
                        $scope.campaignsIndex = _.indexBy(campaigns, "id");
                    })
                campaignModel.bindAll($scope, "campaigns", {
                    brand: brand
                });
            }
        }

        function formatFilterRange(range) {

            console.log(range);

            if (range) {

                var id = $scope.codeFilter.range.id;
                $scope.codeFilter.range = {
                    id: id
                };
            }
        }

        function editBlock(block) {
            if (!!block) {
                $location.path("/codes/bulk/" + block.begin + "/" + block.end);
            }
        }

        function removeFilter(type) {
            if (!!type) delete $scope.codeFilter[type];
        }

        function loadCodes(block) {
            if (!!block) {
                $scope.currentBlock = block;
                codeModel.ejectAll();
                codeModel.findAll({
                        where: {
                            trackingCode: {
                                ">=": block.begin,
                                "<=": block.end
                            }
                        },
                        sort: "trackingCode ASC",
                        limit: 200
                    })
                    .then(function(codes) {
                        $scope.gridOptions.data = formatCodes(codes);
                    })
            }
        }

        function reloadBlocks() {
            codeblockModel.ejectAll();
            codeblockModel.findAll({
                populate: "product,campaign,brand"
            });

            codeblockModel.bindAll($scope, "blocks");
        }

        function onRegisterApi(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {});

        };

        function deleteSelected() {
            var sel = $scope.gridApi.selection.getSelectedRows();
            if (!!sel) sel.forEach(function(item) {

            })
        }

        function formatCodes(codes) {

            codes.forEach(codePopulate);
            codes.forEach(function(code) {
                if (!!code.action) code.actionTick = '&#10004;';
                else code.actionTick = '';
                if (!!code.scan) code.scanTick = '&#10004;';
                else code.scanTick = '';
            })
            $scope.gridOptions.data = codes;

            return codes;
        }

        function codePopulate(code) {
            if (!!code) {
                if (!code.scan) code.scan = false;
                campaignModel.findAll()
                    .then(function(campaigns) {
                        var populatedObj;
                        campaigns.forEach(function(campaign) {
                            if (campaign.id == code.campaign) populatedObj = campaign;
                        })

                        if (!!populatedObj) {
                            code.campaignObj = populatedObj;
                        }
                    });
                userModel.findAll({
                        role: 'brand'
                    })
                    .then(function(brands) {
                        var populatedObj;
                        brands.forEach(function(brand) {
                            if (brand.id == code.brand) populatedObj = brand;
                        })

                        if (!!populatedObj) {
                            code.brandObj = populatedObj;
                        }
                    });
            }
        }

        function handleFileSelect(evt) {

            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.imageEdited = true;
                    $scope.imagetmp = evt.target.result;
                });
            };
            reader.readAsDataURL(file);

        };

        function dataURItoBlob(dataURI, callback) {
            var byteCharacters = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            return new Blob([byteArray], {
                type: mimeString
            });
        }

    }
})();