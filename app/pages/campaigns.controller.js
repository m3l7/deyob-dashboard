(function(){
	angular
		.module('app')
		.controller('Campaigns',Campaigns);

		Campaigns.$inject = ['$scope','$location', 'campaignModel','userModel','security',"lodash","config"];

		function Campaigns($scope,$location, campaignModel,userModel,security,_,config){

			$scope.editSelected = editSelected;
			$scope.deleteSelected = deleteSelected;

			$scope.gridOptions = {
			  enableFiltering: true,
			  showFooter: true,
			  rowHeight: 36,
			  enableRowSelection: true,
			  headerHeight: 77,
			  offsetTop: 75,
			  enableRowHeaderSelection: false,
			  multiSelect: false,
			  onRegisterApi: onRegisterApi,
			  columnDefs: [
			    { name: 'Nome', field:'name' },
			    { name: 'Data di inizio',field: 'dateStart' },
			    { name: 'Data di fine',field:'dateEnd' },
			    { name: 'Tipologia',field:'typeName' },
			  ],
			  data: []
			};

			$scope.gridOptions.noUnselect = true;

			initialize();

			function initialize(){
				//customize for current user role
				if (security.user.role=='admin'){
					$scope.gridOptions.columnDefs.push({
						name: 'Brand',
						field: 'brand.name'
					})
				}

				$scope.campaignTypesIndex = _.indexBy(config.campaignTypes,"type");

				campaignModel.ejectAll();
				campaignModel.findAll({showexpired:true,showAll:true})
				.then(function(campaigns){
					$scope.gridOptions.data = campaigns;

					campaigns.forEach(function(campaign){
						campaign.typeName = $scope.campaignTypesIndex[campaign.type].typeName;
					})
				})
			}

			function onRegisterApi(gridApi){
			      //set gridApi on scope
			      $scope.gridApi = gridApi;
			      gridApi.selection.on.rowSelectionChanged($scope,function(row){});
			 };

			 function editSelected(){
			 	var sel = $scope.gridApi.selection.getSelectedRows();
			 	if (sel.length){
			 		$location.path('/campaigns/'+sel[0].id);
			 	}
			 }
			 function deleteSelected(){
			 	var sel = $scope.gridApi.selection.getSelectedRows();
			 	if (sel.length){
			 		var id = sel[0].id;
			 		campaignModel.destroy(sel[0].id)
			 		.then(function(){
			 			$scope.gridOptions.data.forEach(function(campaign,i){
			 				if (campaign.id==id) $scope.gridOptions.data.splice(i,1);
			 			})
			 		})
			 	}
			 }

			function campaignPopulate(campaign){
			    if (!!campaign){
			        userModel.findAll({role:'brand'})
			        .then(function(brands){
			            var populatedObj;
			            brands.forEach(function(brand){
			                if (brand.id == campaign.brand) populatedObj = brand;
			            })

			            if (!!populatedObj) {
			                campaign.brandObj = populatedObj;
			            }
			        });
			    }
			}

		}
})();