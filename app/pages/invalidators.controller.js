(function(){
	angular
		.module("app")
		.controller("Invalidators",Invalidators);

		Invalidators.$inject = [
			"$scope",
			"userModel",
			"invalidatorModel",
			"$http",
			"config"
		];

		function Invalidators($scope,userModel,invalidatorModel,$http,config){
			$scope.params = {};
			$scope.loadInvalidators = loadInvalidators;
			$scope.gridOptions = getGridOptions();
			$scope.$scope = $scope; //ugly shit for ui grid external scope
			$scope.removeInvalidator = removeInvalidator;
			$scope.findUser = findUser;
			$scope.addInvalidator = addInvalidator;
			
			initialize();

			function initialize(){
				userModel.findAll({role:"brand"});
				userModel.bindAll($scope,"brands",{role:"brand"});
			}	

			function loadInvalidators(brand){
				if (brand){
					invalidatorModel.findAll({brand:brand.id})
					.then(function(invalidators){
						$scope.gridOptions.data = invalidators;
					})
					invalidatorModel.bindAll($scope,"invalidators",{brand:brand.id});
				}
			}

			function removeInvalidator(id){
				if (id){
					invalidatorModel.destroyAll({user:id,brand:$scope.params.brand.id})
					.then(function(){
						$scope.gridOptions.data.forEach(function(item,i){
							if (item.user.id==id) $scope.gridOptions.data.splice(i,1);
						})
						$scope.alert = {type:'success',text:"Invalidatore eliminato con successo"};
					})
				}
			}

			function getGridOptions(){

				return {
					  enableFiltering: true,
					  showFooter: false,
					  enableRowSelection: false,
					  enableRowHeaderSelection: false,
					  columnDefs: [
					    { name: 'username', field:'user.social_username' },
					    { name: 'email',field: 'user.email' },
					    { name: 'nome',field: 'user.first_name' },
					    { name: 'cognome',field: 'user.last_name' },
					    { name: '',width: 50, enableFiltering: false, enableSorting: false, field:'user.id', cellTemplate:'<div class="btn-danger remove-action" ng-click="grid.appScope.removeInvalidator({{COL_FIELD}})"><span>{{getExternalScopes().lol}}</span><i class="glyphicon glyphicon-remove"/></div>'},
					  ],
					  data: []
				};

			}

			function findUser(value){
				if (value){
					return $http.get(config.API.baseUrl+"user/find", {
					      params: {
					      	value: value
					      }
					    }).then(function(response){
				    		response.data.forEach(function(user){
				    			user.fullName = user.social_username + " ("+user.email+")";
				    		})
				    		return response.data;
					    });
				}
			}

			function addInvalidator(){
				if (($scope.params.brand) && ($scope.params.user)){
					var brandId = (typeof $scope.params.brand=="object") ? $scope.params.brand.id : $scope.params.brand;
					var userId = (typeof $scope.params.user=="object") ? $scope.params.user.id : $scope.params.user;
					invalidatorModel.create({brand:brandId,user:userId})
					.then(function(invalidator){
						if (typeof $scope.params.user=="object") invalidator.user = $scope.params.user;
						$scope.gridOptions.data.push(invalidator);
						$scope.alert = {type:'success',text:"Invalidatore aggiunto con successo"};
					})
				}
			}
		}
})();