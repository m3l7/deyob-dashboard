(function(){
	angular
		.module("app")
		.controller("UsersExport",UsersExport);

		UsersExport.$inject = [
			"$scope",
			"userModel",
			"campaignModel",
			"utilUser",
			"$timeout"
		];

		function UsersExport($scope, userModel, campaignModel, utilUser, $timeout){

			$scope.brands = [];
			$scope.campaigns = [];
			$scope.codeParams = {
				brand: "",
				campaign: ""
			};

			//methods
			$scope.exportUsers = exportUsers;
			$scope.reloadCampaigns = reloadCampaigns;
			$scope.resetLink = resetLink;

			initialize();

			function initialize(){
				userModel.findAll({role:"brand"})
				.then(function(brands){

				})
				userModel.bindAll($scope,"brands",{role:"brand"});
			}

			function reloadCampaigns(brandId){
				//reload campaigns after brand is being selected

				if (!brandId) $scope.codeParams.campaign = "";
				else{
					campaignModel.findAll({brand:brandId});
					campaignModel.bindAll($scope,"campaigns",{brand:brandId});
				}

			}

			function exportUsers(){
				//export users, based on codeParams filter
				
				utilUser.getExportUsersLink($scope.codeParams)
				.then(function(link){
					// $scope.exportLink = $sce.trustAsResourceUrl(link);
					$scope.exportLink = link;
				})
				.catch(function(err){
					console.warn("generate export user link: "+err);
				});
		
			}

			function resetLink(){
				$timeout(function(){
					$scope.exportLink = null;
				},50)
			}
		}
})();