(function(){
	angular
		.module("app")
		.controller("Notifications",Notifications);

		Notifications.$inject = [
			"$scope",
			"userModel",
			"campaignModel",
			"$http",
			"config"
		];

		function Notifications($scope, userModel, campaignModel, $http, config){

			$scope.brands = [];
			$scope.campaigns = [];
			$scope.messageParams = {
				brand: "",
				campaign: "",
				text: ""
			};
			$scope.alert = {};
			$scope.loading = false;

			//methods
			$scope.sendPush = sendPush;
			$scope.reloadCampaigns = reloadCampaigns;
			$scope.findUser = findUser;

			initialize();

			function initialize(){
				userModel.findAll({role:"brand"})
				.then(function(brands){

				})
				userModel.bindAll($scope,"brands",{role:"brand"});
			}

			function reloadCampaigns(brandId){
				//reload campaigns after brand is being selected

				if (!brandId) $scope.messageParams.campaign = "";
				else{
					campaignModel.findAll({brand:brandId});
					campaignModel.bindAll($scope,"campaigns",{brand:brandId});
				}

			}

			function findUser(value){
				if (value){
					return $http.get(config.API.baseUrl+"user/find", {
					      params: {
					      	value: value
					      }
					    }).then(function(response){
				    		response.data.forEach(function(user){
				    			user.fullName = user.social_username + " ("+user.email+")";
				    		})
				    		return response.data;
					    });
				}
			}


			function sendPush(){
				var params = angular.copy($scope.messageParams);
				if (params.user) params.user = params.user.id;

				$scope.loading = true;

				$http.post(config.API.baseUrl+"push/send",params)
					.then(function(data){
						$scope.loading = false;
						if (data.status==200) $scope.alert = {type:'success',text:"Notifica push inviata correttamente"};
						else $scope.alert = {type:'error',text:data.data};
					})
			}

		}
})();

