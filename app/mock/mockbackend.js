// Backend mocking with fake data

(function(){
    angular
        .module('app')
        .run(mockBackend);

    mockBackend.$inject = ['$httpBackend','testData','config'];

    function mockBackend($httpBackend,testData,config){
        // BRANDS
           $httpBackend.whenGET(/apiv1\/brand\/(\d+)/).respond(function(method,url,data){
               var id = parseInt(url.split('brand/')[1]);
               var brand = {};
               testData.brands.forEach(function(b){
                    if (b.id==id) brand = b;
               })
               return [200,brand,{}];
           })
           $httpBackend.whenPUT(/apiv1\/brand\/(\d+)/).respond(function(method,url,data){
               var id = parseInt(url.split('brand/')[1]);
               var dataObj = JSON.parse(data);

               testData.brands.forEach(function(b){
                    if (b.id==id) b = dataObj;
               })
               return [200,dataObj,{}];
           })
           $httpBackend.whenGET(/apiv1\/brand/).respond(function(method,url,data){
               return [200,testData.brands,{}];
           })

        // DEFAULT
            $httpBackend.whenGET(/.*/).passThrough();
            $httpBackend.whenPOST(/.*/).passThrough();
            $httpBackend.whenPUT(/.*/).passThrough();
            $httpBackend.whenDELETE(/.*/).passThrough();
    }

})();