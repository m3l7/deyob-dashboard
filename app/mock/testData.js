(function(){

   	var brands = [
   		{
   			id: 1,
   			name: 'MVS Printing',
   			link: "www.mvsprinting.it",
   			phone: "0444021303"
   		},
   		{
   			id: 2,
   			name: 'Pedon Group',
   			link: "www.pedon.it",
   			phone: "0424411151"
   		},
   		{
   			id: 3,
   			name: 'Valbona S.P.A',
   			link: "www.valbona.com",
   			phone: "0429644146"
   		},
   		{
   			id: 4,
   			name: 'Moncler Group',
   			link: "",
   			phone: ""
   		},
   		{
   			id: 5,
   			name: 'I-Tek',
   			link: "www.i-tek.it",
   			phone: "3383048425"
   		},
   		{
   			id: 6,
   			name: 'Lumen srl',
   			location: "Milano",
   			link: "",
   			phone: ""
   		},
   		{
   			id: 7,
   			name: 'Cartografia Veneta S.P.A',
   			location: "Lonigo",
   			link: "www.carven.it",
   			phone: ""
   		},
   		{
   			id: 7,
   			name: 'Cartografia Veneta S.P.A',
   			location: "Lonigo",
   			link: "www.carven.it",
   			phone: ""
   		},
   		{
   			id: 8,
   			name: 'Cartografia Veneta S.P.A',
   			location: "Lonigo",
   			link: "www.carven.it",
   			phone: ""
   		},
   		{
   			id: 9,
   			name: 'Cartografia Veneta S.P.A',
   			location: "Lonigo",
   			link: "www.carven.it",
   			phone: ""
   		},
   		{
   			id: 10,
   			name: 'Cartografia Veneta S.P.A',
   			location: "Lonigo",
   			link: "www.carven.it",
   			phone: ""
   		},
   	];

    var testData = {

    	brands: brands
        
    }

    angular
        .module('app')
        .constant('testData',testData);
})();
