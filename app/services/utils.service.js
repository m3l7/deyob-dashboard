(function(){
	angular
		.module('app')
		.factory('Utils',Utils);

		Utils.$inject = ['$location'];

		function Utils($location){

			return{
				isActive: isActive
			};

			function isActive(path,fixed) {
			  if (fixed==true) return path === $location.path();
			  else if ($location.path().substr(0, path.length) == path) return true;
			  else return false;
			};
		}
})();