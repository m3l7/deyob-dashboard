(function(){
	angular
		.module("app")
		.factory("utilUser",utilUser);

		utilUser.$inject = ["security", "$q", "config", "$http"];

		function utilUser(security, $q, config, $http){
			return {
				getExportUsersLink: getExportUsersLink
			};

			function getExportUsersLink(filter){
				//fetch a short lived token from server and return an export user link

				var filterString = "";
				if ((filter.brand) || (filter.campaign)) filterString = "&where=" + JSON.stringify(filter);

				if ((!security.isAuthenticated) || (security.user.role!="admin")) return $q.reject("User not logged in as admin");
				else return $http.post(config.API.baseUrl+"genToken")
					.then(function(token){
						return config.API.baseUrl+"user/export?access_token="+token.data + filterString;
					})
			}
		}
})();