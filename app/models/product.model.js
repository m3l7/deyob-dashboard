(function(){
    angular
        .module('app')
        .factory('productModel',productModel);

        productModel.$inject = ['DS'];

        function productModel(DS){

            return DS.defineResource({
                name: 'product',
            });
        }
})();