(function(){
    angular
        .module('app')
        .factory('campaignModel',campaignModel);

        campaignModel.$inject = ['DS', "config", "lodash"];

        function campaignModel(DS, config, _){

            return DS.defineResource({
                name: 'campaign',
                beforeInject: function(resource,campaign){

                    //build full name with campaign type name inside

                    var campaignTypesIndex = _.indexBy(config.campaignTypes,"type");

                    campaign.typeName = (campaignTypesIndex[campaign.type]) ? campaignTypesIndex[campaign.type].typeName : "";
                    campaign.fullName = campaign.name + " ("+campaign.typeName+")";

                }

            });
        }
})();