(function(){
    angular
        .module('app')
        .factory('codeblockModel',codeblockModel);

        codeblockModel.$inject = ['DS'];

        function codeblockModel(DS){

            return DS.defineResource({
                name: 'codeblock'
            });
        }
})();