(function(){
    angular
        .module('app')
        .factory('userModel',userModel);

        userModel.$inject = ['DS'];

        function userModel(DS){

            return DS.defineResource({
                name: 'user'
            });
        }
})();