(function(){
    angular
        .module('app')
        .factory('dashboardModel',dashboardModel);

        dashboardModel.$inject = ['DS'];

        function dashboardModel(DS){

            return DS.defineResource({
                name: 'dashboard'
            });
        }
})();