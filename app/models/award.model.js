(function(){
    angular
        .module('app')
        .factory('awardModel',awardModel);

        awardModel.$inject = ['DS'];

        function awardModel(DS){

            return DS.defineResource({
                name: 'award',
                belongsTo:{
                    campaign:{
                        localField: 'campaignObj',
                        localKey: 'campaign',
                    },
                },
            });
        }
})();