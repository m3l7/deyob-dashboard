(function(){
    angular
        .module('app')
        .factory('newsModel',newsModel);

        newsModel.$inject = ['DS'];

        function newsModel(DS){

            return DS.defineResource({
                name: 'news',
            });
        }
})();