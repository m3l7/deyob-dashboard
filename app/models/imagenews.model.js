(function(){
    angular
        .module('app')
        .factory('imagenewsModel',imagenewsModel);

        imagenewsModel.$inject = ['DS'];

        function imagenewsModel(DS){

            return DS.defineResource({
                name: 'imagenews',
            });
        }
})();