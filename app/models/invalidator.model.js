(function(){
    angular
        .module('app')
        .factory('invalidatorModel',invalidatorModel);

        invalidatorModel.$inject = ['DS'];

        function invalidatorModel(DS){

            return DS.defineResource({
                name: 'invalidator'
            });
        }
})();