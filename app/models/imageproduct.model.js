(function(){
    angular
        .module('app')
        .factory('imageproductModel',imageproductModel);

        imageproductModel.$inject = ['DS'];

        function imageproductModel(DS){

            return DS.defineResource({
                name: 'imageproduct',
            });
        }
})();