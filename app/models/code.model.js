(function(){
    angular
        .module('app')
        .factory('codeModel',codeModel);

        codeModel.$inject = ['DS'];

        function codeModel(DS){

            return DS.defineResource({
                name: 'code',
                belongsTo:{
                    campaign:{
                        localField: 'campaignObj',
                        localKey: 'campaign',
                    },
                    user:{
                        localField: 'brandObj',
                        localKey: 'brand',
                    }
                },
            });
        }
})();