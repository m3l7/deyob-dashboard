(function(){
    angular
        .module('app')
        .config(italianLanguage);

        function italianLanguage($translateProvider){

            //set the path for languages
            $translateProvider.useStaticFilesLoader({
              prefix: '/languages/',
              suffix: '.json'
            });

            $translateProvider.translations('it', {

                //ASIDE
                "aside":{
                    "dashboard": "Dashboard",
                    "brands": "Brands",
                    "invalidators": "Invalidatori",
                    "prodotti": "Prodotti",
                    "codici": "Codici",
                    "campagne": "Campagne",
                    "produzione": "Produzione",
                    "milestone": "Milestone",
                    "release": "Release",
                    "news": "Notizie",
                    "export": "Esporta Utenti",
                    "notifications": "Notifiche"
                }


            })            

            $translateProvider.preferredLanguage('it');
        }
})();