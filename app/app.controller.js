(function(){
	angular
		.module('app')
		.controller('App',App);

		App.$inject = ['$scope','config','security'];

		function App($scope,config,security){
			$scope.config = config;
			$scope.security = security;
		}
})();