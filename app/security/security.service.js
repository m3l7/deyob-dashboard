(function(){
  angular
    .module('app')
    .factory('security',phzSecurity);

    phzSecurity.$inject = ['$http','$timeout','$location','config'];

    function phzSecurity($http,$timeout,$location,config){

      var service = {

        logout: logout,
        login: login,
        isAuthenticated: false,
        isAuthorized: isAuthorized,
        getRole: getRole,
        user: {}

      };

      if (localStorage.getItem("token")) fetchLocalStorage();

      function logout(next){
        return $http.post(config.API.baseUrl+'logout')
        .success(function(response){
            localStorage.clear();
            service.user = {};
            service.isAuthenticated = false;
            service.logged = false;
            $http.defaults.headers.common['Authorization']='';
            if (!!next) next();
        })
        .error(function(response){
            if (!!next) next();
        })
      }

      function login(user,pass,next){
        service.logged = false;
        service.isAuthenticated = false;
        clearLocalStorage();
        $http.defaults.headers.common['Authorization']='';

        return $http.post(config.API.baseUrl+'login?username='+user+'&password='+pass)
        .success(function(response){

            if (!!response.token){

              localStorage.setItem("token",response.token);
              localStorage.setItem("uid",response.id);
              localStorage.setItem("user_role",response.role);
              localStorage.setItem("username",response.username);
              localStorage.setItem("name",response.name);
              localStorage.setItem("email",response.email);
              fetchLocalStorage();
              $http.defaults.headers.common['Authorization']='bearer '+localStorage.getItem("token");
            
            }
            if (!!next) next();
        })
        .error(function(response){
          if (!!next) next();
        })
      }
        
      function clearLocalStorage(){
        localStorage.removeItem('token');
        localStorage.removeItem('uid');
        localStorage.removeItem('user_role');
        localStorage.removeItem('username');
        localStorage.removeItem('name');
        localStorage.removeItem('email');
      }

      function fetchLocalStorage(){
        //fill the factory from localstorage.
        service.user = {
          username: localStorage.getItem('username'),
          name: localStorage.getItem('name'),
          role: localStorage.getItem('user_role'),
          uid: localStorage.getItem('uid'),
          email: localStorage.getItem('email'),
        }
        service.token = localStorage.getItem('token');
        service.isAuthenticated = true;
        $http.defaults.headers.common['Authorization']='bearer '+localStorage.getItem("token");
      }

      function isAuthorized(roles){
        //check if the user is authorized for the roles provided
        if (!service.user) return false;
        else if (roles.indexOf(service.user.role)!=-1) return true;
        else return false;
      }
      function getRole(){
        if (!service.isAuthenticated) return "client";
        else if ((!service.user) || (!service.user.role)) return "client";
        else return service.user.role;
      }

      return service;
      
    }

})();