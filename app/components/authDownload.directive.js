(function(){
    angular
        .module('app')
        .directive('fileDownload', function() {
            return {
                restrict: 'EA',
                // templateUrl: '/path/to/pdfDownload.tpl.html',
                template: '<a href="" class="btn btn-primary" ng-click="downloadPdf()">Esporta</a>',
                scope: true,
                link: function(scope, element, attr) {
                    var anchor = element.children()[0];
                    // var anchor = element;
         
                    // When the download starts, disable the link
                    scope.$on('download-start', function() {
                        $(anchor).attr('disabled', 'disabled')
                    });
                    scope.$on('reset', function() {
                        console.log("reset")
                        $(anchor).removeAttr('disabled')
                        .text('Esporta')
                        .attr({href:""})
                    });
         
                    // When the download finishes, attach the data to the link. Enable the link and change its appearance.
                    scope.$on('downloaded', function(event, data) {
                        scope.downloaded = true;
                        $(anchor).attr({
                            href: 'data:'+data.mimetype+';base64,' + data.data,
                            download: attr.filename
                        })
                            .removeAttr('disabled')
                            .text('Download')
                            .removeClass('btn-primary')
                            .addClass('btn-success');
                            

                        // Also overwrite the download pdf function to do nothing.
                        // scope.downloadPdf = function() {

                        // };
                    });

                },
                controller: ['$scope', '$attrs', '$http','$timeout', function($scope, $attrs, $http, $timeout) {
                    $scope.downloadPdf = function() {
                        if ($scope.downloaded){
                            $scope.downloaded = false;

                            $timeout(function(){
                                $scope.$emit('reset');
                            },100)
                        }
                        else{
                            $scope.$emit('download-start');
                            $http.get($attrs.url).then(function(response) {
                                if (response.status==200) $scope.$emit('downloaded', response.data);
                                else $scope.$emit('reset');
                            });
                        }
                    };
                }]
            } 
        });
})();