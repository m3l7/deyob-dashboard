(function(){
    angular
        .module('app')
        .controller('EditAwardModal',EditAwardModal);

        EditAwardModal.$inject = ['$scope','$modalInstance','award',"campaign",'config']

        function EditAwardModal($scope,$modalInstance,award,campaign,config){

            $scope.award = award;
            $scope.campaign = campaign;
            $scope.formSubmitted = false;
            $scope.imageEdited = false;
            $scope.formImage = '';
            $scope.formCroppedImage = '';
            $scope.initialize = initialize;

            $scope.ok = function (valid) {
                $scope.formSubmitted = true;
                if ($scope.imageEdited) {
                    $scope.award.blob = $scope.formCroppedImage;
                }
                if (valid) $modalInstance.close($scope.award);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function handleFileSelect(evt) {
              var file=evt.currentTarget.files[0];
              var reader = new FileReader();
              reader.onload = function (evt) {
                $scope.$apply(function($scope){
                  $scope.imageEdited = true;
                  $scope.formImage=evt.target.result;
                });
              };
              // console.log($scope.myImage)
              reader.readAsDataURL(file);

            };

            function initialize(){
                angular.element(document.querySelector('#awardFileInput')).on('change',handleFileSelect);
            }

        }
})();