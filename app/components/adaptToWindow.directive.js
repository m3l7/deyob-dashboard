(function(){
	angular
		.module('app')
		.directive('adaptToWindow',adaptToWindow);

		adaptToWindow.$inject = ['$window','$timeout'];

		function adaptToWindow($window,$timeout){
			var directive = {
				link: link,
				restrict: 'EA',
				replace: false
			};

			return directive;

			function link(scope,element,attrs){

				scope.$watch('gridOptions.data',function(newv,oldv){

					var rowHeight = scope.gridOptions.rowHeight; // your row height
					var headerHeight = scope.gridOptions.headerHeight; // your row height

					var computedHeight = (scope.gridOptions.data.length * rowHeight + headerHeight);
					var windowHeight = $window.innerHeight - scope.gridOptions.offsetTop - 70;
					var height = (computedHeight>windowHeight) ? windowHeight : computedHeight;

					element.attr('style','height: '+height+'px');


				})

			}
		}
})();