(function(){
	angular
		.module('app')
		.directive('alertMessage',alertMessage);

		alertMessage.$inject = ['$timeout'];

		function alertMessage($timeout){
			var directive = {
				link: link,
				restrict: 'EA',
				scope:{
					message: '='
				},
				replace: 'true',
				template: "<div class='alert alert-message alert-{{message.type}}' ng-class='{&quot;active&quot;:!!message.text}'>"+
					'<span ng-show="!!message.text" type="{{message.type}}" close="closeAlert($index)">{{message.text}}</span>'+
					'<span ng-show="!!message.errorCode"> (<strong>{{message.errorCode}}</strong>)</span>'
			};

			return directive;

			function link(scope,element,attrs){
				scope.$watch('message.text',function(newm,oldm){
					if (!!newm){
						var timeout = scope.message.timeout || 5000;
						$timeout(function(){
							delete scope.message.text;
							delete scope.message.errorCode;
						},timeout)
					}
				})
			}
		}

})();