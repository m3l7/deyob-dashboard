(function(){
    var config = {
        logging:{
          http: true
        },
        API:{
          //main server
          // baseUrl: 'http://localhost:14050/apiv1/', //localhost
          baseUrl: 'http://52.25.62.75:80/apiv1/', //deyob unstable
          // baseUrl: 'http://52.25.62.75:14050/apiv1/', //deyob test
          // baseUrl: 'http://api.deyob.com/apiv1/', //deyob production

          //secondary server for codes generation
          // baseUrlGenerateCodes: 'http://localhost:14050/apiv1/', //localhost
          baseUrlGenerateCodes: 'http://52.25.62.75:80/apiv1/', //deyob unstable
          // baseUrlGenerateCodes: 'http://52.27.167.111:14050/apiv1/', //deyob production secondary (gen codes)
          // baseUrlGenerateCodes: 'http://api.deyob.com/apiv1/', //deyob production 

          idAttribute: 'id',
        },
        lang:{
          langs: ['it','en','de','fr'],
          defaultLang: 'it'
        },
        url:{
          prefix: '/#!'
        },
        theme:{
          asideColor: 'bg-black'
        },
        aside:{
          folded: false
        },
        campaignTypes:[
          {
            type: 1,
            typeName: 'Estrazione'
          },
          {
            type: 3,
            typeName: 'Raccolta Punti'
          },
          {
            type: 4,
            typeName: 'Carta Fedeltà'
          },
          {
            type: 2,
            typeName: 'Instant Win'
          },
          {
            type: 5,
            typeName: 'Virale'
          },
        ],
        viralTypes:[
          {
            value: 1,
            name: "Mother Code"
          },
          {
            value: 2,
            name: "Spot Code"
          }
        ],
        actionTypes:[
          {
            name: "Youtube link",
            value: 1
          },
          {
            name: "Testo e immagine",
            value: 2
          },
          {
            name: "Nessuna",
            value: 0
          },
        ]
    }

    angular
        .module('app')
        .constant('config',config)
        .config(['$locationProvider', function($location) {
          $location.hashPrefix('!');
        }]);
})();
