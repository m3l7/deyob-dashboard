// Log HTTP requests to browser's console

(function(){

    angular
        .module('app')
        .factory('logHttpInterceptor',logHttpInterceptor);
    angular
        .module('app')
        .config(configInterceptor);

    logHttpInterceptor.$inject = ['$log','$q', 'config', 'logService']

    function logHttpInterceptor($log,$q,config,logService){
        return {
            request: function(req){
                if (config.logging.http){
                    var params = '';

                    if (!!req.params) {
                        var params = '?';
                        Object.keys(req.params).forEach(function(k){
                            params += k+"="+JSON.stringify(req.params[k])+"&";
                        })
                        params = params.substring(0, params.length - 1);
                    }
                    $log.info(req.method+": "+req.url+params); 
                    if (!!req.data) $log.log(JSON.stringify(req.data));
                } 
                return req;
            },
            response: function(response){
                if (!!response.data.summary) logService.log.error(response.data.summary);
                else if (response.status=='401') logService.log.error('401: Unauthorized');
                else if (response.status=='404') logService.log.error('404: Resource not found');
                else if (response.status=='500') logService.log.error('Errore del server');
                else if ((response.status!="200") && (response.status!="201")) logService.log.error('Errore del server: '+response.data);
                return response;
            },
            responseError: function(res){
                // debugger
            }
        };
    }

    function configInterceptor($httpProvider){
        $httpProvider.interceptors.push('logHttpInterceptor');
    }
})();