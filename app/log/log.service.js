(function(){
		angular
			.module('app')
			.factory('logService',logService);

			logService.$inject = ['$log', 'config'];

			function logService($log,config){
				function logError(text){
					$log.error(text);
					service.lastMessage.type = 'danger';
					if (!!text.summary){
						service.lastMessage.text = text.summary;
						service.lastMessage.errorCode = text.errorCode;
					}
					else{
						service.lastMessage.text = text;
						delete service.lastMessage.errorCode;
					}
				}

				var service = {
					log: {
						error: logError,
						//COMPLETE ME!
					},
					lastMessage:{}
				}	

				return service;
			}
})();