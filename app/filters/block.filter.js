(function(){
	angular
		.module('app')
		.filter('block', block);

		function block() {
		  return function (items,filter) {
		  	var blocks = [];
		  	if (items.length) items.forEach(function(block){
		  		var valid = true;
			  	if ((filter.campaign) && (block.campaign) && (block.campaign.id!=filter.campaign.id)) valid = false;
			  	if ((filter.brand) && (block.campaign) && (block.brand.id!=filter.brand.id)) valid = false;
			  	if (valid) blocks.push(block);
		  	})
		  	return blocks;
		  };
		};

})();